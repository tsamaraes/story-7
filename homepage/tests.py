from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.urls import resolve
from .models import Message
from .forms import MessageForm
from .views import index, confirmation
from django.apps import apps
from homepage.apps import HomepageConfig
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


class Story7UnitTest(TestCase):
    def test_story7_index_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_story7_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_story7_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_story7_confirmation_url_is_exist(self):
        response = Client().get('/confirmation')
        self.assertEqual(response.status_code,200)

    def test_story7_confirmation_template(self):
        response = Client().get('/confirmation')
        self.assertTemplateUsed(response, 'confirmation.html')

    def test_story7_confirmation_func(self):
        found = resolve('/confirmation')
        self.assertEqual(found.func, confirmation)

    def test_create_new_message(self):
        new = Message.objects.create(name = 'Sarah', message = 'hello')
        self.assertTrue(isinstance(new, Message))
        self.assertTrue(new.__str__(), new.message)
        count_message = Message.objects.all().count()
        self.assertEqual(count_message, 1)

    def test_display_message(self):
        message = "semangat"
        data = {'message' : message}
        post_data = Client().post('/', data)
        self.assertEqual(post_data.status_code, 200)

    def test_application(self):
        self.assertEqual(HomepageConfig.name, 'homepage')
        self.assertEqual(apps.get_app_config('homepage').name, 'homepage')

    def test_index_content(self):
        response = Client().get('/')
        response_content = response.content.decode('utf-8')
        self.assertIn("Hello. How are you today?", response_content)
    
    def test_confirmation_content(self):
        response = Client().get('/confirmation')
        response_content = response.content.decode('utf-8')
        self.assertIn("Are you sure?", response_content)

    def test_data_confirmation_page_post(self) :
        response = Client().post('', {'name': 'Sarah', 'message': 'yuk bisa yuk'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'confirmation.html')

    def test_data_validation(self) :
        response = self.client.post('/confirmation', data = {'name':'Sarah', 'message':'hello there'})
        count = Message.objects.all().count()
        self.assertEqual(count, 1)
        self.assertEqual(response.status_code, 302)
        added_data = self.client.get('/')
        added_data_content = added_data.content.decode('utf-8')
        self.assertIn('Sarah', added_data_content)
        self.assertIn('hello there', added_data_content)

class Story7FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story7FunctionalTest,self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story7FunctionalTest,self).tearDown()
    
    def test_input_data_then_confirm(self):
        self.selenium.get(self.live_server_url)
        response_content = self.selenium.page_source

        # User inputs data to the form
        self.selenium.find_element_by_name('name').send_keys('Sarah')
        self.selenium.find_element_by_name('message').send_keys('Hello there')
        self.selenium.find_element_by_name('postmessage').click()

        # User validates and agree to post their status
        name = self.selenium.find_element_by_name('name').get_attribute('value')
        message = self.selenium.find_element_by_name('message').get_attribute('value')
        self.assertIn('Sarah', name)
        self.assertIn('Hello there', message)
        self.selenium.find_element_by_name('yes').click()
        
        # User status is shown on the page
        response_content = self.selenium.page_source
        self.assertIn('Sarah', response_content)
        self.assertIn('Hello there', response_content)

        # CHALLENGE
        # Test to change functionality of change font color button
        old_message_color = self.selenium.find_element_by_class_name('target').value_of_css_property('color')
        self.assertEqual(old_message_color, 'rgba(33, 37, 41, 1)')

        self.selenium.find_element_by_id('changeColor').click()

        new_message_color = self.selenium.find_element_by_class_name('target').value_of_css_property('color')
        self.assertEqual(new_message_color, 'rgba(237, 102, 99, 0.8)')

    def test_input_data_then_cancel(self):
        self.selenium.get(self.live_server_url)
        response_content = self.selenium.page_source

        # User inputs data to the form
        self.selenium.find_element_by_name('name').send_keys('Sarah')
        self.selenium.find_element_by_name('message').send_keys('Hello there')
        self.selenium.find_element_by_name('postmessage').click()

        # User doesn't want to post their status
        name = self.selenium.find_element_by_name('name').get_attribute('value')
        status = self.selenium.find_element_by_name('message').get_attribute('value')
        self.assertIn('Sarah', name)
        self.assertIn('Hello there', status)
        self.selenium.find_element_by_name('no').click()

        # User's status is not shown on the page
        self.assertIn('Mood Board', response_content)
