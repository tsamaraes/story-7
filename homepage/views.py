from django.shortcuts import render
from django.shortcuts import redirect
from .models import Message
from . import forms
from .forms import MessageForm

# Create your views here.
def index(request):
    message = Message.objects.all()
    if request.method == "POST":
        form = MessageForm(request.POST)
        if (form.is_valid()):
            user_name = form.cleaned_data['name']
            user_message = form.cleaned_data['message']
            response = {'name':user_name, 'message':user_message}
            return render(request, 'confirmation.html', response)
    else:
        form = MessageForm()
    return render(request, 'index.html', {'message' : message})

def confirmation(request):
    if request.method == "POST":
        form = MessageForm(request.POST)
        if (form.is_valid()):
            name = form.cleaned_data['name']
            message = form.cleaned_data['message']
            new_message = Message.objects.create(name=name, message=message)
            new_message.save()
            return redirect('homepage:index')
    else:
        form = MessageForm()
    return render(request, 'confirmation.html')