from django.db import models
from datetime import datetime

class Message(models.Model):
    name= models.CharField('name', max_length = 100)
    message = models.TextField('message', max_length = 100)

    def __str__(self):
        return self.name