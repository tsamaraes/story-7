from django import forms
from .models import Message

class MessageForm(forms.Form):
    name = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : "Write your name here",
        'type' : 'text',
        'maxlength' : '100',
        'required' : True,
        'label' : '',
    }))
    message = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : "I'm feeling...",
        'type' : 'text',
        'maxlength' : '100',
        'required' : True,
        'label' : '',
    }))

